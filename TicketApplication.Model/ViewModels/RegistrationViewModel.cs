﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicketApplication.Model
{
    [NotMapped]
    public class RegistrationViewModel
    {

        public int ID { get; set; }

        public string Name { get; set; }

        public string EmailID { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public int? RoleID { get; set; }

        public string CreatedOn { get; set; }
    }
}

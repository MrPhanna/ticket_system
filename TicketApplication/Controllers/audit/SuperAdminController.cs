﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TicketApplication.Core;
// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TicketApplication.Controllers.audit
{
    public class SuperAdminController : Controller
    {
        // GET: /<controller>/
        public IActionResult Home()
        {
            return View();
        }
    }
}

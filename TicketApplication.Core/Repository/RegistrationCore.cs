﻿
using TicketApplication.Model;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;

namespace TicketApplication.Core
{
    public class RegistrationCore : IRegistration
    {
        private DatabaseContext _context;

        public RegistrationCore(DatabaseContext context)
        {
            _context = context;
        }

        public void AddAdmin(Registration entity)
        {
            _context.Registration.Add(entity);
            _context.SaveChanges();
        }

        public int AddUser(Registration entity)
        {
            _context.Registration.Add(entity);
            return _context.SaveChanges();
        }

        public bool CheckUserNameExists(string Username)
        {
            var result = (from user in _context.Registration
                          where user.Username == Username
                          select user).Count();

            if (result > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public RegistrationViewModel Userinformation(int UserID)
        {
            var result = (from user in _context.Registration
                          where user.ID == UserID
                          select new RegistrationViewModel
                          {
                              Name = user.Name,
                              EmailID = user.EmailID,
                              CreatedOn = user.CreatedOn.Value.ToString("dd/MM/yyyy"),

                              Username = user.Username,
                              Password = user.Password,
                          }).SingleOrDefault();
            return result;
        }

        public IQueryable<RegistrationViewModel> UserinformationList(string sortColumn, string sortColumnDir, string Search)
        {
            var IQueryableReg = (from user in _context.Registration
                                 select new RegistrationViewModel                                {

                                     Name = user.Name,
                                     EmailID = user.EmailID,
                                     CreatedOn = user.CreatedOn.Value.ToString("dd/MM/yyyy"),
                                     Username = user.Username
                                 });
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
            {
                IQueryableReg = IQueryableReg.OrderBy(sortColumn + " " + sortColumnDir);
            }
            if (!string.IsNullOrEmpty(Search))
            {
                IQueryableReg = IQueryableReg.Where(m => m.Username == Search || m.EmailID == Search);
            }

            return IQueryableReg;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicketApplication.Core
{
    public interface IRoles
    {
        int getRolesofUserbyRolename(string Rolename);
    }
}

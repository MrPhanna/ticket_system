﻿using TicketApplication.Model;
using Microsoft.EntityFrameworkCore;

namespace TicketApplication.Core
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {

        }

        public DbSet<Registration> Registration { get; set; }
        public DbSet<Roles> Roles { get; set; }
        
    }
}
